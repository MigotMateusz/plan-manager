from django.contrib.auth.decorators import login_required
from django.db import connection
from django.shortcuts import redirect, render

from .models import Note


@login_required
def index(request):
    cursor = connection.cursor()
    try:
        cursor.execute("SELECT TEXT FROM public.\"profilePanel_note\" WHERE USER_ID = " + str(request.user.id))
        query = cursor.fetchall()
    finally:
        cursor.close()
    return render(request, 'profile.html', {'query': query})


@login_required
def saveNote(request):
    if request.method == "POST":
        text = request.POST.get('note')
        note = Note(text=text, user=request.user)
        note.save()
    return redirect("index")
