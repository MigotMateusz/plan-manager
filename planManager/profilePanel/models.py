from django.conf import settings
from django.db import models


class Note(models.Model):
    text = models.CharField(max_length=3000)
    user = models.ForeignKey(settings.AUTH_USER_MODEL,
                             on_delete=models.CASCADE,
                             verbose_name='Owner of the note')
