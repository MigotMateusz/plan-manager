from django.urls import path

from . import views
from timetableWizard import views as views2

urlpatterns = [
    path('', views.creatorPanel, name='creatorPanel'),
    path('saveGroupName/', views.saveGroupName, name='saveGroup'),
    path('removeStudent/', views.removeStudent, name='removeStudent'),
    path('addStudent/', views.addStudent, name='addStudent'),
    path('deleteGroup/', views.deleteGroup, name='deleteGroup'),
    path('timetable/', views2.timetableWizard, name='timetableWizard')
]
