from django.db import models
from django.conf import settings


class Group(models.Model):
    group_name = models.CharField(max_length=25)
    users = models.ManyToManyField(settings.AUTH_USER_MODEL)
    objects = models.Manager()

