from django.contrib.auth.decorators import login_required
from django.shortcuts import redirect, render
from django.db import connection
from sign.decorators import creator_required
from .models import Group
from django.contrib.auth import get_user_model


@login_required
@creator_required
def creatorPanel(request):
    cursor = connection.cursor()
    try:
        cursor.execute("SELECT id, first_name, last_name, email FROM public.sign_user WHERE is_student=true")
        students = cursor.fetchall()
        cursor.execute("""SELECT groups.id, groups.group_name, 
            array_agg(groups.id || ',' || users.id || ',' || users.first_name || ' ' || users.last_name)
            FROM public."creatorPanel_group" groups
            left join public."creatorPanel_group_users" gu
            on groups.id = gu.group_id
            left join public.sign_user users
            on gu.user_id = users.id
            group by groups.id""")
        groups = prepareStudentsToRemove(cursor.fetchall())
        studentsOutOfGroup = getStudentsOutOfGroup(groups, students)
        groups = combineStudents(groups, studentsOutOfGroup)
        cursor.execute("""SELECT users.id, users.first_name, users.last_name, users.email,
            array_agg(groups.group_name)
            FROM public."sign_user" users
            left join public."creatorPanel_group_users" gu
            on users.id = gu.user_id
            left join public."creatorPanel_group" groups
            on gu.group_id = groups.id
            WHERE is_student=true
            group by users.id""")
        groupsOfStudent = removeNullValue(cursor.fetchall())
    finally:
        cursor.close()
    return render(request, 'creator.html', {'groups': groups, 'groupsOfStudent': groupsOfStudent})


def saveGroupName(request):
    if request.method == "POST":
        name = request.POST.get('group')
        if Group.objects.filter(group_name=name).exists():
            raise Exception("Group with this name already exists")
        group = Group()
        group.group_name = name
        group.save()
    return redirect("creatorPanel")


def removeStudent(request):
    if request.method == "POST":
        students = request.POST.getlist('studentsToBeRemoved')
        User = get_user_model()
        for student in students:
            student = student[1:(len(student)-1)]
            student = student.replace("'", "")
            studentTuple = tuple(map(str, student.split(', ')))
            groupID = int(studentTuple[0])
            userID = int(studentTuple[1])
            user = User.objects.get(id=userID)
            group = Group.objects.get(id=groupID)
            group.users.remove(user)
    return redirect("creatorPanel")


def deleteGroup(request):
    groups = request.POST.getlist('groupsToBeDeleted')
    for group in groups:
        Group.objects.filter(id=int(group)).delete()
    return redirect("creatorPanel")


def addStudent(request):
    if request.method == "POST":
        students = request.POST.getlist('studentsToBeAdded')
        User = get_user_model()
        for student in students:
            student = student[1:(len(student) - 1)]
            student = student.replace("'", "")
            studentTuple = tuple(map(str, student.split(', ')))
            user = User.objects.get(id=int(studentTuple[1]))
            group = Group.objects.get(id=int(studentTuple[0]))
            group.users.add(user)
    return redirect("creatorPanel")


def removeNullValue(groupsOfStudent):
    result = []
    for x in groupsOfStudent:
        lista = x[4]
        res = []
        for val in lista:
            if val is not None:
                res.append(val)
        result.append(x[:4] + (res,))
    return result


def prepareStudentsToRemove(groups):
    result = []
    for group in groups:
        value = group[2]
        res = (group[0],) + (group[1],)
        lista = []
        for string in value:
            if string is None:
                break
            else:
                lista.append(tuple(tuple(string.split(','))))
        res += (lista,)
        result.append(res)
    return result


def getStudentsOutOfGroup(groups, students):
    studentIDList = []
    result = []
    for student in students:
        studentIDList.append(int(student[0]))
    for group in groups:
        res = (group[0],) + (group[1],)
        studentsIdsOutOfGroup = studentIDList.copy()
        value = group[2]
        if len(value) != 0:
            for student in value:
                userID = int(student[1])
                try:
                    studentsIdsOutOfGroup.remove(userID)
                except:
                    print("Hippity hoppity your error is my property")

        result.append(studentsIdsOutOfGroup)
    return prepareStudentsToAdd(groups, result, students)


def prepareStudentsToAdd(groups, studentsOutOfGroup, students):
    result = []
    i = 0
    for group in groups:
        lista = []
        studentsIdsOutOfGroup = studentsOutOfGroup[i].copy()
        res = (group[0],) + (group[1],)
        for student in students:
            if int(student[0]) in studentsIdsOutOfGroup:
                groupID = group[0]
                studentID = student[0]
                tup = (groupID,) + (studentID,) + (student[1] + " " + student[2],)
                lista.append(tuple(tup))

        res += (lista,)
        result.append(res)
        i += 1
    return result


def combineStudents(groups, studentsOutOfGroup):
    i = 0
    res = []
    for group in groups:
        temp = studentsOutOfGroup[i]
        group += (temp[2],)
        i += 1
        res.append(group)
    return res