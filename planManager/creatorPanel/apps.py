from django.apps import AppConfig


class CreatorpanelConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "creatorPanel"
