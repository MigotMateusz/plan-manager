from creatorPanel.models import Group
from django.contrib.auth.decorators import login_required
from django.db import connection
from django.shortcuts import render
from sign.decorators import student_required
from timetableWizard.models import Timetable


# Create your views here.

@login_required
@student_required
def index(request):
    cursor = connection.cursor()
    cursor.execute("SELECT group_id FROM public.\"creatorPanel_group_users\" WHERE user_id = " + str(request.user.id))
    query = cursor.fetchall()

    group_ids = []

    for x in query:
        group_ids.append(x[0])

    group = list(Group.objects.filter(id__in=group_ids).values_list())
    timetables = list(Timetable.objects.filter(group__in=group).order_by('day_of_the_week', 'parity', 'start').values_list())
    timetables = mutateData(timetables)

    return render(request, 'student.html', {'timetables': timetables})


def mutateData(timetables):
    result = []

    for x in timetables:
        y = list(x)

        match y[4]:
            case 0:
                y[4] = "Monday"
            case 1:
                y[4] = "Tuesday"
            case 2:
                y[4] = "Wednesday"
            case 3:
                y[4] = "Thursday"
            case 4:
                y[4] = "Friday"
            case 5:
                y[4] = "Saturday"
            case 6:
                y[4] = "Sunday"

        if y[5]:
            y[5] = "Odd"
        else:
            y[5] = "Even"

        x = tuple(y)
        result.append(x)

    return result


