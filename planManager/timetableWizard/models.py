from django.db import models

from creatorPanel.models import Group


class Timetable(models.Model):
    name = models.CharField(max_length=50)
    start = models.TimeField()
    duration = models.IntegerField()
    day_of_the_week = models.IntegerField()
    parity = models.BooleanField()
    instructor = models.CharField(max_length=30)
    classroom = models.CharField(max_length=20)
    group = models.ManyToManyField(Group)
