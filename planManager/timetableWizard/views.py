from django.contrib.auth.decorators import login_required
from django.contrib.sessions import serializers
from django.shortcuts import render, redirect

from creatorPanel.models import Group
from sign.decorators import creator_required
from timetableWizard.models import Timetable


@login_required
@creator_required
def timetableWizard(request):
    if request.method == "POST":
        groupId = request.POST.get("group_id")
        request.session['group_id'] = groupId

    else:
        groupId = request.session.get('group_id')

    group = Group.objects.get(id=request.session.get('group_id'))
    timetables = list(Timetable.objects.filter(group=group).order_by('day_of_the_week', 'parity', 'start').values_list())
    timetables = mutateData(timetables)

    return render(request, 'timetable.html', {'group_name': group.group_name, 'timetables': timetables, 'group_id': groupId})


@login_required
@creator_required
def addLesson(request):
    if request.method == "POST":
        name = request.POST.get("name")
        starttime = request.POST.get("starttime")
        duration = request.POST.get("duration")
        dayoftheweek = request.POST.get("dayoftheweek")
        parity = (request.POST.get("parity") == "odd")
        instructor = request.POST.get("instructor")
        classroom = request.POST.get("classroom")

        group = Group.objects.get(id=request.session.get('group_id'))

        timetable = Timetable.objects.create(name=name, start=starttime, duration=duration,
                                             day_of_the_week=dayoftheweek,
                                             parity=parity, instructor=instructor, classroom=classroom)
        timetable.group.add(group)
        timetable.save()

    return redirect("timetableWizard")


@login_required
@creator_required
def delLesson(request):
    if request.method == "POST":
        print("---------------------------DEL--------------------------")
        lesson_id = request.POST.get("lesson_id")
        timetable = Timetable.objects.get(id=lesson_id)
        timetable.delete()

    return redirect("timetableWizard")


def mutateData(timetables):
    result = []

    for x in timetables:
        y = list(x)

        match y[4]:
            case 0:
                y[4] = "Monday"
            case 1:
                y[4] = "Tuesday"
            case 2:
                y[4] = "Wednesday"
            case 3:
                y[4] = "Thursday"
            case 4:
                y[4] = "Friday"
            case 5:
                y[4] = "Saturday"
            case 6:
                y[4] = "Sunday"

        if y[5]:
            y[5] = "Odd"
        else:
            y[5] = "Even"

        x = tuple(y)
        result.append(x)

    return result




