from django.apps import AppConfig


class TimetablewizardConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'timetableWizard'
