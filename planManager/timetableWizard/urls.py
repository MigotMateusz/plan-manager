from django.urls import path

from . import views

urlpatterns = [
    path('', views.timetableWizard, name='timetableWizard'),
    path('addLesson/', views.addLesson, name='addLesson'),
    path('delLesson/', views.delLesson, name='delLesson')
]