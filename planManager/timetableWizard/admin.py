from django.contrib import admin

from timetableWizard.models import Timetable

admin.site.register(Timetable)
