from django import forms

from django.contrib.auth.forms import UserCreationForm
from django.db.transaction import commit

from .models import User


class RegistrationForm(UserCreationForm):
    firstname = forms.CharField(required=True)
    lastname = forms.CharField(required=True)
    username = forms.CharField(required=True)
    email = forms.EmailField(required=True)
    password1 = forms.CharField(widget=forms.PasswordInput, required=True)
    password2 = forms.CharField(widget=forms.PasswordInput, required=True)
    is_student = forms.BooleanField(initial=True)
    is_creator = forms.BooleanField(initial=False)

    class Meta:
        model = User
        fields = ['firstname', 'lastname', 'username', 'email', 'password1', 'password2', 'is_student', 'is_creator']

    def save(self):
        user = super().save(commit=False)
        user.set_password(self.cleaned_data["password1"])
        if commit:
            user.save()
        return user
