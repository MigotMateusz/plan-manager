from django.shortcuts import render
from django.contrib import messages, auth

from .forms import RegistrationForm
from django.shortcuts import redirect
from .models import User


# Create your views here.
def register(request):
    if request.method == "POST":
        form = RegistrationForm()
        if form.is_valid():
            form.save()

        first_name = request.POST.get('firstname')
        last_name = request.POST.get('lastname')
        email = request.POST.get('email')
        username = request.POST.get('username')
        password1 = request.POST.get('password1')
        password2 = request.POST.get('password2')

        if len(password1) < 6:
            messages.error(request, '⚠️ Password should be at least 6 characters for greater security')

            return render(request, 'registration/register.html')

        if password1 != password2:
            messages.error(request, '⚠️ Password Mismatch! Your Passwords Do Not Match')

            return render(request, 'registration/register.html')

        if not email:
            messages.error(request, '⚠️ Email is required')

            return render(request, 'registration/register.html')

        if not username:
            messages.error(request, '⚠️ Username is required!')

            return render(request, 'registration/register.html')

        if User.objects.filter(username=username).exists():
            messages.error(request, '⚠️ Username is taken! Choose another one')

            return render(request, 'registration/register.html')

        if User.objects.filter(email=email).exists():
            messages.error(request, '⚠️ Email is taken! Choose another one')

            return render(request, 'registration/register.html')

        user = User.objects.create_user(first_name=first_name, last_name=last_name, username=username, email=email,
                                        is_student=True, is_creator=False)
        user.set_password(password1)
        user.save()
        return redirect("main")
    else:
        form = RegistrationForm()
    return render(request, 'registration/register.html', {'form': form})


def index(request):
    if request.method == 'POST':
        username = request.POST['username']
        password = request.POST['password']

        user = auth.authenticate(username=username, password=password)

        if user is not None:
            auth.login(request, user)
        else:
            messages.info(request, 'invalid username or password')
            return redirect("/")
    else:
        return render(request, 'registration/login.html')
