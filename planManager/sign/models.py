from django.contrib.auth.models import AbstractUser
from django.db import models


class User(AbstractUser):
    is_student = models.BooleanField('Is student', default=True)
    is_creator = models.BooleanField('Is creator', default=False)
    USERNAME_FIELD = 'username'
