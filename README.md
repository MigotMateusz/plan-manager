# Plan-manager

## App:
* Python
* Django
* Postgres 

>Uwierzytelnianie i autoryzacja za pomocą funkcjonalności Django.

### Strona internetowa z planem zajęć składająca się z:

**Panelu kreatora**
- może tworzyć/usuwać grupy
- może dodawać/usuwać studentów z grupy
- może tworzyć plany zajęć

**Panelu studenta**
- ma podgląd do swojego planu zajęć
- może dodawać informacje prywatne w postaci notatek na swoim profilu

**Panelu admina**
- może zarządzać użytkownikami i grupami (wygenerowany przez Django)

> Mając postawioną bazę postgres można odpalić projekt u siebie.
>> Dostępny jest panel logowania i rejestracji użytkowników. 
Domyślnie nowy użytkownik jest typu "student". Admin może nadawać uprawnienia kreatora.

#### Do panelów mają dostęp zalogowani użytkownicy o określonych uprawnieniach.
#### Aplikacja rozpoznaje użytkowników (admin, kreator, student).

> Instrukcja odpalenia projektu:
>> 1) W pliku settings.py ustawić odpowiednie połączenie z postgres
>> 2) w terminalu:
>> * python manage.py makemigrations
>> * python manage.py migrate 
>> * python manage.py runserver 
>> 3) http://127.0.0.1:8000/ 

